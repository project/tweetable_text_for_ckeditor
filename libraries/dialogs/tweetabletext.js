CKEDITOR.dialog.add('tweetabletextDialog', function(editor) {
  return {
    title: 'TweetableText Properties',
    minWidth: 400,
    minHeight: 200,
    contents: [
      {
        id: 'tab-basic',
        label: 'Basic Settings',
        elements: [
          {
            type: 'text',
            id: 'displaytext',
            label: 'Display Text',
            placeholder:'sample',
            validate: CKEDITOR.dialog.validate.notEmpty('Display Text field can not be empty.'),
            setup: function(element) {
              this.setValue(element.getText());

            },
            commit: function(element) {
              element.setValue(this.getValue());

            }
          },
          {
            type: 'text',
            id: 'tweetabletext',
            label: 'Tweetable Text',
            maxLength: 280,
            validate: CKEDITOR.dialog.validate.notEmpty('Tweetable Text field can not be empty.'),
            setup: function(element) {
              // var splitURL = element.getAttribute('href').split("?");
              // var dataTweet = splitURL[1].split("=");
              // var decodedDataTweet = decodeURI(dataTweet[1]);
              // this.setValue(decodedDataTweet);
              this.setValue(element.getAttribute('data-tweet'));
            },
            commit: function(element) {
/*               var twitterBaseUrl = 'http://twitter.com/intent/tweet?';
              var encodedValue = encodeURI(this.getValue());
              twitterBaseUrl += 'text=' + encodedValue;
              element.setAttribute('href', twitterBaseUrl); */

              // For some reason, without doing this the changes won't be reflected on frontend.
              element.setAttribute('data-tweet', this.getValue());
              // element.setText(element.getAttribute('data-hash'));
            }
          },
          {
            type: 'text',
            id: 'tweethashtags',
            label: 'Tweetable Text Hash tags (#sample #news)',
            // validate: CKEDITOR.dialog.validate.notEmpty('Tweetable Text field can not be empty.'),
            setup: function(element) {
              // this.setValue(element.getText());
              this.setValue(element.getAttribute('data-hash'));

            },
            commit: function(element) {
              element.setAttribute('data-hash', this.getValue());
 
            }
          }
        ]
      }
    ],

    onShow: function() {
      var selection = editor.getSelection();
      var element = selection.getStartElement();

      if (element.getAttribute('class') !== 'tweetabletext') {
        element = editor.document.createElement('span');
        this.insertMode = true;
      }
      else {  
        this.insertMode = false;
      }

      this.element = element;
      if (!this.insertMode) {
        this.setupContent(this.element);
      }
    },

    onOk: function() {
      var dialog = this;
      var retrieveElement = this.element;

      var getDisplayText = dialog.getValueOf('tab-basic', 'displaytext');
      var getTweetableText = dialog.getValueOf('tab-basic', 'tweetabletext');
      var getTweetHashtags = dialog.getValueOf('tab-basic', 'tweethashtags');
      // var getTweetableText = encodeURI(getTweetableText);

      // var twitterBaseUrl = 'http://twitter.com/intent/tweet?';
      var tweetabletext = editor.document.createElement('span');
      var tweetabletextAfter = editor.document.createElement('span');
      tweetabletext.setAttribute('class', 'tweetabletext');

      // twitterBaseUrl += 'text=' + getTweetableText;
      tweetabletext.setAttribute('data-tweet', getTweetableText);
      tweetabletext.setAttribute('data-hash', getTweetHashtags);
      // tweetabletext.setAttribute('data-href', twitterBaseUrl);
      tweetabletext.setText(getDisplayText);

      this.commitContent(retrieveElement);

      if (this.insertMode) {
        editor.insertElement(tweetabletext);
        editor.insertElement(tweetabletextAfter);
      }
    }
  };
});
