/* global drupalSettings, jQuery */
/* eslint no-undef: "error" */
(function ($) {
  // Helper function to interpolate template.
  function interpolate(str, obj) {
    let parts = str.split(/\$\{(?!\d)[\wæøåÆØÅ]*\}/);
    let args = str.match(/[^{\}]+(?=})/g) || [];
    let parameters = args.map(argument => obj[argument] || (obj[argument] === undefined ? "" : obj[argument]));
    return String.raw({ raw: parts }, ...parameters);
  }
  $('span.tweetabletext').each(function (i, e) {
    var text = $(e).text();
    var tweet = $(e).attr('data-tweet');
    var tweetHash = $(e).attr('data-hash');
    if (!tweet) {
      tweet = '';
    }
    if (!tweetHash) {
      tweetHash = '';
    }
    tweet = tweet.replace(/\s/g, '+');
    tweetHash = tweetHash.replace(/\s/g, ',');
    tweetHash = tweetHash.replace(/\#/g, '');
    var totalLength = tweet + ' ' + tweetHash + window.location.href;
    totalLength = totalLength.length;
    if (totalLength > 276) {
      var excess = totalLength - (240 + 3)
      tweet = tweet.substring(0, tweet.length - excess) + '...';
    }
    const replacements = {
      tweet_text: tweet,
      page_url: window.location.href,
      hash_tags: tweetHash,
      display_text: text,
      tweetable_logo: '<span id="tweetablelogo"></span>',
    };
      
    var template = drupalSettings.tweetable_text_ckeditor.template;
    template = $.parseHTML((interpolate(template, replacements)));
      
    var color = drupalSettings.tweetable_text_ckeditor.color;
    if (color) {
      $(template).css('background', color);
    }
    $(e).html(template)
  })
}(jQuery));
