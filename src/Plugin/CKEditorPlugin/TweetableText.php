<?php

namespace Drupal\tweetable_text_ckeditor\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "tweetabletext" plugin.
 *
 * @CKEditorPlugin(
 *   id = "tweetabletext",
 *   label = @Translation("CKEditor Panel Button"),
 * )
 */
class TweetableText extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    $path = \Drupal::service('extension.list.module')->getPath('tweetable_text_ckeditor') . '/libraries/plugin.js';
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'TweetableText' => [
        'label' => $this->t('Tweetable Text'),
        'image' => \Drupal::service('extension.list.module')->getPath('tweetable_text_ckeditor') . '/icons/tweetabletext.png',
      ],
    ];
  }

}
