<?php

namespace Drupal\tweetable_text_ckeditor\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;

/**
 * Configure example settings for this site.
 */
class TweetabletextSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'tweetable_text_ckeditor.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tweetable_text_ckeditor_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    // $color = $config->get('tweetable_text_ckeditor_color');
    $form['tweetable_text_ckeditor_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tweetable text color'),
      '#default_value' => $config->get('tweetable_text_ckeditor_color'),
      '#description' => 'Leave blank for none.',
    ];

    $form['tweetable_text_ckeditor_template'] = array(
      '#type' => 'textarea',
      '#title' => t('Template'),
      '#default_value' => $config->get('tweetable_text_ckeditor_template'),
      '#description' => 'Use following variables: <i>${tweet_text}</i>, <i>${page_url}</i>, <i>${hash_tags}</i>, <i>${display_text}</i>, <i>${tweetable_logo}</i>.',
      '#rows' => 4,
      '#required' => TRUE,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('tweetable_text_ckeditor_color', $form_state->getValue('tweetable_text_ckeditor_color'))
      ->set('tweetable_text_ckeditor_template', Xss::filter($form_state->getValue('tweetable_text_ckeditor_template')))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
