CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Provides an input filter for making text in content
tweetable, just by clicking on the sentence or phrase itself.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/tweetable_text_for_ckeditor

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/tweetable_text_for_ckeditor



INSTALLATION
------------

 * Install the Tweetable text CKEditor as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Content Authoring > Text
       formats and editors and configure the desired profile.
    3. Drag the Twitter button into the Active toolbar and save the
       configuration.
    4. Clear your browser's cache, and a new Twitter button will appear in your
       CKEditor toolbar.


MAINTAINERS
-----------

Module created by:
 *  Vishnu Kumar - https://www.drupal.org/u/vishnukumar

